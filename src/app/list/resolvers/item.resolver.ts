import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn } from '@angular/router';
import { IUser } from '../interfaces/user';
import { UsersService } from '../services/users.service';

export const ItemResolver: ResolveFn<IUser> =
  (route: ActivatedRouteSnapshot) => inject(UsersService).getUser(route.params['id']);
