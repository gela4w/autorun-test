import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list.component';
import { DetailsComponent } from './components/details/details.component';
import { ItemResolver } from './resolvers/item.resolver';
import { RoutePath } from '../shared/enums/route-paths';

const routes: Routes = [
  {
    path: RoutePath.Root,
    children: [
      {
        path: RoutePath.Root,
        component: ListComponent,
      },
      {
        path: 'item-details/:id',
        component: DetailsComponent,
        resolve: { item: ItemResolver }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListRoutingModule {}
