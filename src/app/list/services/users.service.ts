import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUser } from '../interfaces/user';

@Injectable()
export class UsersService {
  users: IUser[] = [];

  private httpClient = inject(HttpClient);
  private baseUserUrl = 'https://jsonplaceholder.typicode.com/users';

  getUsers() {
    const subscription = this.httpClient.get<IUser[]>(this.baseUserUrl)
      .subscribe(res => {
        this.users = res;
        subscription.unsubscribe();
      });
  }

  loadNextUsers() {
    //explicitly limit the number of users for infinite scroll
    //as JSONPlaceholder returns only 10 users by default
    if(this.users.length <= 50) {
      const subscription = this.httpClient.get<IUser[]>(this.baseUserUrl)
        .subscribe(res => {
          this.users = [...this.users, ...res];
          subscription.unsubscribe();
        });
    }
  }
  getUser(id: string) {
    return this.httpClient.get<IUser>(`${this.baseUserUrl}/${id}`)
  }
}
