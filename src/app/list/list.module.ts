import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { ListComponent } from './list.component';
import { ListRoutingModule } from './list-routing.module';
import { DetailsComponent } from './components/details/details.component';
import { UsersService } from './services/users.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [CardComponent, ListComponent, DetailsComponent],
  imports: [CommonModule, ListRoutingModule, InfiniteScrollModule],
  providers: [UsersService]
})
export class ListModule {}
