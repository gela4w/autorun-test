import { Component, Input } from '@angular/core';
import { IUser } from '../../interfaces/user';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutePath } from '../../../shared/enums/route-paths';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() user: IUser;

  constructor(private router: Router, private route: ActivatedRoute) {}


  navigateToDetails() {
    this.router.navigate([RoutePath.Details, this.user.id], { relativeTo: this.route })
  }
}
