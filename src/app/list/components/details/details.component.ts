import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IUser } from '../../interfaces/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent  implements OnInit, OnDestroy {
  user: IUser;

  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.subscription = this.activatedRoute.data
      .subscribe(({ item }) => this.user = <IUser>item);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  get address(): string {
    const { address } = this.user;

    return `${address.zipcode}, ${address.city}, ${address.street}, ${address.suite}`
  }
}
