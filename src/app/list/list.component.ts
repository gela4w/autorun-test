import { Component } from '@angular/core';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent {
  constructor(public usersService: UsersService) {
    this.usersService.getUsers();
  }

  onScroll() {
    this.usersService.loadNextUsers();
  }
}
