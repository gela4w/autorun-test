import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutePath } from './shared/enums/route-paths';
import { LayoutComponent } from './shared/components/layout/layout.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: RoutePath.Root,
    component: LayoutComponent,
    children: [
      {
        path: RoutePath.Root,
        loadChildren: () => import('./list/list.module').then(({ ListModule }) => ListModule),
      },
      {
        path: RoutePath.Table,
        loadChildren: () => import('./table/table.module').then(({ TableModule }) => TableModule),
      },
    ],
  },
  {
    path: RoutePath.Wildcard,
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
