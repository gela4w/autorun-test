import { TableColumns } from '../../shared/enums/table-columns';

export interface ITableColumnData {
  displayName: string;
  columnName: TableColumns;
}
