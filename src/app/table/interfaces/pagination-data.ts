export interface IPaginationData<T> {
  items: T[];
  count: number;
}
