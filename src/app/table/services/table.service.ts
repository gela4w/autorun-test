import { Injectable } from '@angular/core';
import { IPost } from '../interfaces/post';
import { TableDataService } from './table-data.service';
import { SortOrder } from '../../shared/enums/sort-order';
import { BehaviorSubject, combineLatest, debounceTime, map, Observable, switchMap, tap } from 'rxjs';
import { TableColumns } from '../../shared/enums/table-columns';

@Injectable()
export class TableService {
  private readonly pageSize = 10;
  private readonly debounceTime = 50;
  private pagesCount = 0;

  posts$: Observable<IPost[]>;
  currentPage$ = new BehaviorSubject<number>(1);
  searchTerm$ = new BehaviorSubject<string>('');
  sortBy$ = new BehaviorSubject<TableColumns>(TableColumns.Number);
  order$ = new BehaviorSubject<SortOrder>(SortOrder.ASC);

  constructor(private dataService: TableDataService) {
    this.posts$ = combineLatest([this.currentPage$, this.searchTerm$, this.sortBy$, this.order$])
      .pipe(debounceTime(this.debounceTime))
      .pipe(switchMap(([currentPage, searchTerm, sortBy, order]) =>
        this.loadData(currentPage, searchTerm, sortBy, order)));
  }

  loadPreviousPage() {
    const previousPage = this.currentPage$.value - 1;

    if(previousPage) {
      this.currentPage$.next(previousPage);
    }
  }

  loadNextPage() {
    const nextPage = this.currentPage$.value + 1;

    if(nextPage <= this.pagesCount) {
      this.currentPage$.next(nextPage);
    }
  }

  onSearch(searchTerm) {
    this.searchTerm$.next(searchTerm);
    this.currentPage$.next(1);
  }

  onSort(columnName: TableColumns) {
    this.toggleSortOrder(columnName);
    this.sortBy$.next(columnName);
  }

  get pages(): number[] {
    const PAGES_OFFSET = 2;
    const currentPage = this.currentPage$.value;
    const pages = [];
    const minPage = Math.max(1, currentPage - PAGES_OFFSET);
    const maxPage = Math.min(this.pagesCount, currentPage + PAGES_OFFSET);

    for (let i = minPage; i <= maxPage; i++) {
      pages.push(i);
    }
    return pages;
  }

  get isFirstPage(): boolean {
    return this.currentPage$.value === 1;
  }

  get isLastPage(): boolean {
    return this.currentPage$.value === this.pagesCount;
  }

  refreshTable() {
    this.currentPage$.next(this.currentPage$.value);
  }

  private loadData(currentPage, searchTerm, sortBy, order): Observable<IPost[]> {
    return this.dataService
      .getPosts(currentPage, this.pageSize, searchTerm, sortBy, order)
      .pipe(tap(({ count }) => this.pagesCount = Math.ceil(count / this.pageSize)))
      .pipe(map(({ items }) => items));
  }

  private toggleSortOrder(columnName: TableColumns) {
    if(columnName === this.sortBy$.value) {
      this.order$.next(this.order$.value === SortOrder.ASC ? SortOrder.DESC : SortOrder.ASC);
    } else {
      this.order$.next(SortOrder.ASC);
    }
  }
}
