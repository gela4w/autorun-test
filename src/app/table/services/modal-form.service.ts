import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IPost } from '../interfaces/post';

const TITLE_MAX_LENGTH = 100;
const BODY_MAX_LENGTH = 255;

@Injectable()
export class ModalFormService {
  postForm = new FormGroup({
    id: new FormControl(''),
    number: new FormControl(null),
    title: new FormControl('', [Validators.required, Validators.maxLength(TITLE_MAX_LENGTH)]),
    body: new FormControl('', [Validators.required, Validators.maxLength(BODY_MAX_LENGTH)]),
  });

  fillForm({ id, title , body, number }: IPost) {
    this.postForm.setValue({ id, title , body, number });
  }
}
