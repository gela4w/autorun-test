import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPost } from '../interfaces/post';
import { Observable } from 'rxjs';
import { IPaginationData } from '../interfaces/pagination-data';

@Injectable()
export class TableDataService {
  private readonly URL = 'https://6529692955b137ddc83ec445.mockapi.io/posts';

  constructor(private httpClient: HttpClient) {}

  getPosts(page, limit, search, sortBy, order): Observable<IPaginationData<IPost>> {
    return this.httpClient
      .get<IPaginationData<IPost>>(this.URL, { params: { page, limit, search, sortBy, order } });
  }

  updatePost(post) {
    return this.httpClient.put(`${this.URL}/${post.id}`, post);
  }
}
