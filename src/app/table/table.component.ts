import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TableService } from './services/table.service';
import { TableDataService } from './services/table-data.service';
import { ModalComponent } from './components/modal/modal.component';
import { IPost } from './interfaces/post';
import { TableColumns } from '../shared/enums/table-columns';
import { ModalFormService } from './services/modal-form.service';
import { ITableColumnData } from './interfaces/table-column-data';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [TableService, TableDataService, ModalFormService]
})
export class TableComponent implements OnInit, OnDestroy {
  @ViewChild('modal') modal: ModalComponent;

  columns: ITableColumnData[] = [
    { columnName: TableColumns.Number, displayName: '#' },
    { columnName: TableColumns.Title, displayName: 'Title' },
    { columnName: TableColumns.Body, displayName: 'Body' },
  ];
  posts: IPost[];

  private subscription: Subscription;

  constructor(public tableService: TableService) {}

  ngOnInit() {
    this.subscription = this.tableService.posts$
      .subscribe(_posts => this.posts = _posts);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openModal(post: IPost) {
    this.modal.show(post);
  }

  closeModal() {
    this.modal.hide();
  }

  getModalHeader(): string {
    return `Post # ${this.modal?.item?.number}`;
  }
}
