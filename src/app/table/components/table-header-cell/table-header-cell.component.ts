import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SortOrder } from '../../../shared/enums/sort-order';
import { ITableColumnData } from '../../interfaces/table-column-data';
import { TableColumns } from '../../../shared/enums/table-columns';

@Component({
  selector: 'app-table-header-cell',
  templateUrl: './table-header-cell.component.html',
  styleUrls: ['./table-header-cell.component.scss']
})
export class TableHeaderCellComponent {
  @Input() column: ITableColumnData;
  @Input() sortBy: string;
  @Input() order: SortOrder;

  @Output() onSort = new EventEmitter<TableColumns>();

  get sortIcon(): string {
    return this.order === SortOrder.ASC ? 'bi-sort-alpha-down' : 'bi-sort-alpha-down-alt';
  }

  get isSortIconDisplayed(): boolean {
    return this.column.columnName === this.sortBy;
  }

  onClick() {
    this.onSort.emit(this.column.columnName);
  }
}
