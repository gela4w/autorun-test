import { AfterViewInit, Component, EventEmitter, HostListener, Input, Output, TemplateRef } from '@angular/core';
import { Modal } from 'bootstrap';
import { IPost } from '../../interfaces/post';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements AfterViewInit {
  @Input() templateRef: TemplateRef<any>;
  @Input() modalHeader: string = '';
  @Output() onClose = new EventEmitter();

  @HostListener('hidden.bs.modal') onHide() {
    this.onClose.emit();
  }

  private modal: Modal;

  item: any;

  ngAfterViewInit() {
    this.modal = new Modal('#modal');
  }

  show(post: IPost) {
    this.item = post;
    this.modal.show();
  }

  hide() {
    this.modal.hide();
  }
}
