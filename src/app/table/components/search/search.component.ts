import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { debounceTime, distinctUntilChanged, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  private readonly debounceTime = 400;

  @Output() searchTermChange = new EventEmitter<string>();

  searchTermSubject = new Subject<string>();

  private subscription: Subscription;

  ngOnInit() {
    this.subscription = this.searchTermSubject
      .pipe(debounceTime(this.debounceTime))
      .pipe(distinctUntilChanged())
      .subscribe(searchTerm => this.searchTermChange.emit(searchTerm));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getSearchTerm(event: Event): string {
    return (event.target as HTMLInputElement).value;
  }
}
