import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { ModalFormService } from '../../services/modal-form.service';
import { TableDataService } from '../../services/table-data.service';
import { IPost } from '../../interfaces/post';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss']
})
export class ModalFormComponent implements OnChanges, OnDestroy {
  @Input() item: IPost;
  @Output() onFormSubmit = new EventEmitter();

  private subscription: Subscription;

  constructor(public formService: ModalFormService, private dataService: TableDataService) {}

  ngOnChanges(changes: SimpleChanges) {
    if('item' in changes && this.item) {
      this.formService.fillForm(this.item);
    }
  }

  onSubmit() {
    if(this.formService.postForm.valid) {
      this.subscription = this.dataService.updatePost(this.formService.postForm.value)
        .subscribe(() => this.onFormSubmit.emit());
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  getControlErrors(controlName: string): ValidationErrors {
    return this.formService.postForm.get(controlName)?.errors;
  }
}
