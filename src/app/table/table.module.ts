import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableRoutingModule } from './table-routing.module';
import { TableComponent } from './table.component';
import { SearchComponent } from './components/search/search.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { ModalComponent } from './components/modal/modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalFormComponent } from './components/modal-form/modal-form.component';
import { SharedModule } from '../shared/shared.module';
import { TableHeaderCellComponent } from './components/table-header-cell/table-header-cell.component';


@NgModule({
  declarations: [
    TableComponent,
    SearchComponent,
    PaginationComponent,
    ModalComponent,
    ModalFormComponent,
    TableHeaderCellComponent
  ],
  imports: [
    CommonModule,
    TableRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class TableModule {}
