export enum RoutePath {
  Root = '',
  Table = 'table',
  Details = 'item-details',
  Wildcard = '**',
}
