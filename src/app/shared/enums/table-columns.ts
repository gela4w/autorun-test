export enum TableColumns {
  Number = 'number',
  Title = 'title',
  Body = 'body',
}
