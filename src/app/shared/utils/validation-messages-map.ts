import { ValidationErrorType } from '../enums/validation-error-type';

export const validationMessagesMap = {
  [ValidationErrorType.MaxLength]: ({ actualLength, requiredLength }) =>
    `This field can not exceed ${requiredLength} characters. Current length is ${actualLength} characters`,
  [ValidationErrorType.Required]: () => 'This field is required',
};
