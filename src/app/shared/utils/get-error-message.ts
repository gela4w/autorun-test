import { validationMessagesMap } from './validation-messages-map';
import { ValidationErrors } from '@angular/forms';

export function getErrorMessage(errors: ValidationErrors): string {
  return Object.keys(errors)
    .map(key => (validationMessagesMap[key] ? validationMessagesMap[key](errors[key]) : null))
    .find(error => Boolean(error));
}
