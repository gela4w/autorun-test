import { Component } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `<h1>Oops, something went wrong :(</h1>`,
})
export class PageNotFoundComponent {}
