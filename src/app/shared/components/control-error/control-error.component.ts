import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { getErrorMessage } from '../../utils/get-error-message';

@Component({
  selector: 'app-control-error',
  templateUrl: './control-error.component.html',
  styleUrls: ['./control-error.component.scss']
})
export class ControlErrorComponent {
  @Input() errors: ValidationErrors;

  get errorMessage(): string | null {
    if(this.errors) {
      return getErrorMessage(this.errors);
    }
    return null;
  }
}
