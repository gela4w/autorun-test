import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { NavComponent } from './components/nav/nav.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ControlErrorComponent } from './components/control-error/control-error.component';

@NgModule({
  declarations: [LayoutComponent, NavComponent, PageNotFoundComponent, ControlErrorComponent],
  imports: [CommonModule, RouterModule],
  exports: [ControlErrorComponent]
})
export class SharedModule {}
